/**
 * Calculate the area of a cicle
 * @param {number} radius of the cirle
 * @returns the area if this cirle
 */
function cirleArea(radius) {
  return radius * radius * Math.PI;
  // console.log(x * x * 3.14);
}

const cirleAreaArrow = (radius) => {
  return radius * radius * Math.PI;
};

// only 1 line
const cirleAreaArrowSimple = radius => radius * radius * Math.PI;

console.log(cirleArea(3));
console.log(cirleAreaArrow(3));
console.log(cirleAreaArrowSimple(3));

// function dt(x) {
//   console.log(x * x * 3.14);
// }
// dt(3);
