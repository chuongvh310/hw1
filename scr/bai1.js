// const - khai bao mot hang so - gia tri khong thay doi duoc trong suot qua trinh chay
// keyword: javascript hoisting
// let, const -> scope ({}), prefer use const
// var: less prefer

const a = 5;
console.log(a);
// a = 10;  loi
// let - bien chi co the truy cap duoc trong block bao quanh no
function foo1() {
  let x = 10;
  if (true) {
    // let, const
    let x = 20; // x nay la x khac o tren
    console.log(x);
  }
  console.log(x);
}
// var - bien co pham vi chay xuyen suot function chua no
function foo2() {
  var x = 10;
  if (true) {
    x = 20; // x nay la x  o tren
    console.log(x);
  }
  console.log(x);
}

foo1();
foo2();
