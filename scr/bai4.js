function getLetter(s) {
  switch (s.charAt(0)) {
    case 'a':
    case 'e':
    case 'i':
    case 'o':
    case 'u':
      return 'A';
    case 'b':
    case 'c':
    case 'd':
    case 'f':
    case 'g':
      return 'B';
    case 'h':
    case 'j':
    case 'k':
    case 'l':
    case 'm':
      console.log('B');
      break;
    case 'n':
    case 'p':
    case 'q':
    case 'r':
    case 's':
    case 't':
    case 'v':
    case 'w':
    case 'x':
    case 'y':
    case 'z':
      console.log('C');
      break;
    default:
      console.log('Error');
      break;
  }
}
getLetter('string');
